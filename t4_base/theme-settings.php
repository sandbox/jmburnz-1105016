<?php
// Implimentation of hook_form_system_theme_settings_alter().
function t4_form_system_theme_settings_alter(&$form, $form_state) {

  // Add some CSS so we can style our form in any theme, i.e. in Seven.
  drupal_add_css(drupal_get_path('theme', 't4') . '/css/theme-settings.css', array('group' => CSS_THEME));
  
  // Build a form for the custom theme settings using Drupals Form API.
  // Layout settings
  $form['t4'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => -10,
    '#default_tab' => 'defaults',
  );
  $form['t4']['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout'),
    '#weight' => -100,
  );
  // layout type, fluid or fixed
  $form['t4']['layout']['layout_type'] = array(
    '#type' => 'select',
    '#title' => t('Layout type'),
    '#default_value' => theme_get_setting('layout_type'),
    '#options' => array(
      'fixed' => t('Fixed width page'),
      'fluid' => t('Fluid width page'),
    )
  );
  $form['t4']['layout']['layout_width_fixed'] = array(
    '#type' => 'select',
    '#title' => t('Page width'),
    '#default_value' => theme_get_setting('layout_width_fixed'),
    '#options' => array(
      '960px' => t('720px'),
      '960px' => t('780px'),
      '960px' => t('840px'),
      '960px' => t('900px'),
      '960px' => t('960px'),
      '1020px' => t('1020px'),
      '1080px' => t('1080px'),
      '1140px' => t('1140px'),
      '1200px' => t('1200px'),
      '1260px' => t('1260px'),
      '1260px' => t('1320px'),
    ),
    '#states' => array(
      'visible' => array(
          '#edit-layout-type' => array(
            'value' => t('fixed'),
          ),
      ),
    ),
  );
  $form['t4']['layout']['layout_width_fluid'] = array(
    '#type' => 'select',
    '#title' => t('Page width'),
    '#default_value' => theme_get_setting('layout_width_fluid'),
    '#options' => array(
      '80%' => t('80%'),
      '85%' => t('85%'),
      '90%' => t('90%'),
      '95%' => t('95%'),
      '98%' => t('98%'),
      '100%' => t('100%'),
    ),
    '#states' => array(
      'visible' => array(
          '#edit-layout-type' => array(
            'value' => t('fluid'),
          ),
      ),
    ),
  );
  // min/max toggle
  $form['t4']['layout']['layout_minmax'] = array(
    '#type' => 'fieldset',
    '#title' => t('Min and Mix Widths'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#states' => array(
      'visible' => array(
        '#edit-layout-width-fluid' => array('value' => t('100%')),
        '#edit-layout-type' => array('value' => t('fluid')),
      ),
    ),
  );
  $form['t4']['layout']['layout_minmax']['layout_minmax_toggle'] = array(
    '#type' => 'checkbox',
    '#title' => '<strong>' . t('Set min and max widths: ') . '</strong>' . '<span class="description">' . t('this allows the page to stretch betten a minimum and maximum width, knows as a <em>flexible layout</em>.') . '</span>',
    '#default_value' => theme_get_setting('layout_minmax_toggle'),
    '#states' => array(
      'visible' => array(
        '#edit-layout-width-fluid' => array(
          'value' => t('100%'),
        ),
      ),
    ),
  );
  $form['t4']['layout']['layout_minmax']['layout_min_width'] = array(
    '#type'  => 'select',
    '#title' => t('Min width'),
    '#default_value' => theme_get_setting('layout_min_width'),
    '#options' => array(
      '960px' => t('720px'),
      '960px' => t('780px'),
      '960px' => t('840px'),
      '960px' => t('900px'),
      '960px' => t('960px'),
      '1020px' => t('1020px'),
      '1080px' => t('1080px'),
      '1140px' => t('1140px'),
      '1200px' => t('1200px'),
      '1260px' => t('1260px'),
    ),
    '#states' => array(
      'visible' => array(
        "input[name='layout_minmax_toggle']" => array("checked" => TRUE),
      ),
    ),
  );
  $form['t4']['layout']['layout_minmax']['layout_max_width'] = array(
    '#type'  => 'select',
    '#title' => t('Max width'),
    '#default_value' => theme_get_setting('layout_max_width'),
    '#options' => array(
      '960px' => t('780px'),
      '960px' => t('840px'),
      '960px' => t('900px'),
      '960px' => t('960px'),
      '1020px' => t('1020px'),
      '1080px' => t('1080px'),
      '1140px' => t('1140px'),
      '1200px' => t('1200px'),
      '1260px' => t('1260px'),
      '1260px' => t('1320px'),
    ),
    '#states' => array(
      'visible' => array(
        "input[name='layout_minmax_toggle']" => array("checked" => TRUE),
      ),
    ),
  );
  // sidebar widths
  $form['t4']['layout']['sidebar_first_width'] = array(
    '#type'  => 'select',
    '#title' => t('Sidebar first width'),
    '#default_value' => theme_get_setting('sidebar_first_width'),
    '#options' => array(
      '1' => t('1 grid column'),
      '2' => t('2 grid columns'),
      '3' => t('3 grid columns'),
      '4' => t('4 grid columns'),
      '5' => t('5 grid columns'),
      '6' => t('6 grid columns'),
      '7' => t('7 grid columns'),
      '8' => t('8 grid columns'),
      '9' => t('9 grid columns'),
      '10' => t('10 grid columns'),
      '11' => t('11 grid columns'),
      '12' => t('12 grid columns'),
    ),
  );
  $form['t4']['layout']['sidebar_second_width'] = array(
    '#type'  => 'select',
    '#title' => t('Sidebar second width'),
    '#default_value' => theme_get_setting('sidebar_second_width'),
    '#options' => array(
      '1' => t('1 grid column'),
      '2' => t('2 grid columns'),
      '3' => t('3 grid columns'),
      '4' => t('4 grid columns'),
      '5' => t('5 grid columns'),
      '6' => t('6 grid columns'),
      '7' => t('7 grid columns'),
      '8' => t('8 grid columns'),
      '9' => t('9 grid columns'),
      '10' => t('10 grid columns'),
      '11' => t('11 grid columns'),
      '12' => t('12 grid columns'),
    ),
  );
  // sidebar_layout_enabled can only be enabled in the subthemes info file
  if (theme_get_setting('sidebar_layout_enabled') == '1') {
    $form['t4']['layout']['sidebar_layout'] = array(
      '#type'  => 'radios',
      '#title' => t('Sidebar layout'),
      '#default_value' => theme_get_setting('sidebar_layout'),
      '#options' => array(
        '0' => t('Split sidebars: sidebar-content-sidebar'),
        '1' => t('Sidebars left: content-sidebar-sidebar'),
      ),
    );
  }
  $form['t4']['layout']['layout_debug_grid'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug Grid'),
    '#default_value' => theme_get_setting('layout_debug_grid'),
  );
  // Fonts
  $form['t4']['font'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fonts and Headings'),
    '#weight' => -90,
  );
  $form['t4']['font']['base_font'] = array(
    '#type' => 'select',
    '#title' => t('Base font'),
    '#default_value' => theme_get_setting('base_font'),
    '#options' => array(
      'bf-sss' => t('Helvetica Nueue, Trebuchet MS, Arial, Nimbus Sans L, FreeSans, sans-serif'),
      'bf-ssl' => t('Verdana, Geneva, Arial, Helvetica, sans-serif'),
      'bf-a'   => t('Arial, Helvetica, sans-serif'),
      'bf-ss'  => t('Garamond, Perpetua, Nimbus Roman No9 L, Times New Roman, serif'),
      'bf-sl'  => t('Baskerville, Georgia, Palatino, Palatino Linotype, Book Antiqua, URW Palladio L, serif'),
      'bf-m'   => t('Myriad Pro, Myriad, Arial, Helvetica, sans-serif'),
      'bf-l'   => t('Lucida Sans, Lucida Grande, Lucida Sans Unicode, Verdana, Geneva, sans-serif'),
    ),
  );
  $form['t4']['font']['site_name_font'] = array(
    '#type' => 'select',
    '#title' => t('Site Name'),
    '#default_value' => theme_get_setting('site_name_font'),
    '#options' => array(
      'snf-sss' => t('Helvetica Nueue, Trebuchet MS, Arial, Nimbus Sans L, FreeSans, sans-serif'),
      'snf-ssl' => t('Verdana, Geneva, Arial, Helvetica, sans-serif'),
      'snf-a'   => t('Arial, Helvetica, sans-serif'),
      'snf-ss'  => t('Garamond, Perpetua, Nimbus Roman No9 L, Times New Roman, serif'),
      'snf-sl'  => t('Baskerville, Georgia, Palatino, Palatino Linotype, Book Antiqua, URW Palladio L, serif'),
      'snf-m'   => t('Myriad Pro, Myriad, Arial, Helvetica, sans-serif'),
      'snf-l'   => t('Lucida Sans, Lucida Grande, Lucida Sans Unicode, Verdana, Geneva, sans-serif'),
    ),
  );
  $form['t4']['font']['site_slogan_font'] = array(
    '#type' => 'select',
    '#title' => t('Site Slogan'),
    '#default_value' => theme_get_setting('site_slogan_font'),
    '#options' => array(
      'ssf-sss' => t('Helvetica Nueue, Trebuchet MS, Arial, Nimbus Sans L, FreeSans, sans-serif'),
      'ssf-ssl' => t('Verdana, Geneva, Arial, Helvetica, sans-serif'),
      'ssf-a'   => t('Arial, Helvetica, sans-serif'),
      'ssf-ss'  => t('Garamond, Perpetua, Nimbus Roman No9 L, Times New Roman, serif'),
      'ssf-sl'  => t('Baskerville, Georgia, Palatino, Palatino Linotype, Book Antiqua, URW Palladio L, serif'),
      'ssf-m'   => t('Myriad Pro, Myriad, Arial, Helvetica, sans-serif'),
      'ssf-l'   => t('Lucida Sans, Lucida Grande, Lucida Sans Unicode, Verdana, Geneva, sans-serif'),
    ),
  );
  $form['t4']['font']['page_title_font'] = array(
    '#type' => 'select',
    '#title' => t('Page title'),
    '#default_value' => theme_get_setting('page_title_font'),
    '#options' => array(
      'ptf-sss' => t('Helvetica Nueue, Trebuchet MS, Arial, Nimbus Sans L, FreeSans, sans-serif'),
      'ptf-ssl' => t('Verdana, Geneva, Arial, Helvetica, sans-serif'),
      'ptf-a'   => t('Arial, Helvetica, sans-serif'),
      'ptf-ss'  => t('Garamond, Perpetua, Nimbus Roman No9 L, Times New Roman, serif'),
      'ptf-sl'  => t('Baskerville, Georgia, Palatino, Palatino Linotype, Book Antiqua, URW Palladio L, serif'),
      'ptf-m'   => t('Myriad Pro, Myriad, Arial, Helvetica, sans-serif'),
      'ptf-l'   => t('Lucida Sans, Lucida Grande, Lucida Sans Unicode, Verdana, Geneva, sans-serif'),
    ),
  );
  $form['t4']['font']['node_title_font'] = array(
    '#type' => 'select',
    '#title' => t('Node titles'),
    '#default_value' => theme_get_setting('node_title_font'),
    '#options' => array(
      'ntf-sss' => t('Helvetica Nueue, Trebuchet MS, Arial, Nimbus Sans L, FreeSans, sans-serif'),
      'ntf-ssl' => t('Verdana, Geneva, Arial, Helvetica, sans-serif'),
      'ntf-a'   => t('Arial, Helvetica, sans-serif'),
      'ntf-ss'  => t('Garamond, Perpetua, Nimbus Roman No9 L, Times New Roman, serif'),
      'ntf-sl'  => t('Baskerville, Georgia, Palatino, Palatino Linotype, Book Antiqua, URW Palladio L, serif'),
      'ntf-m'   => t('Myriad Pro, Myriad, Arial, Helvetica, sans-serif'),
      'ntf-l'   => t('Lucida Sans, Lucida Grande, Lucida Sans Unicode, Verdana, Geneva, sans-serif'),
    ),
  );
  $form['t4']['font']['comment_title_font'] = array(
    '#type' => 'select',
    '#title' => t('Comment titles'),
    '#default_value' => theme_get_setting('comment_title_font'),
    '#options' => array(
      'ctf-sss' => t('Helvetica Nueue, Trebuchet MS, Arial, Nimbus Sans L, FreeSans, sans-serif'),
      'ctf-ssl' => t('Verdana, Geneva, Arial, Helvetica, sans-serif'),
      'ctf-a'   => t('Arial, Helvetica, sans-serif'),
      'ctf-ss'  => t('Garamond, Perpetua, Nimbus Roman No9 L, Times New Roman, serif'),
      'ctf-sl'  => t('Baskerville, Georgia, Palatino, Palatino Linotype, Book Antiqua, URW Palladio L, serif'),
      'ctf-m'   => t('Myriad Pro, Myriad, Arial, Helvetica, sans-serif'),
      'ctf-l'   => t('Lucida Sans, Lucida Grande, Lucida Sans Unicode, Verdana, Geneva, sans-serif'),
    ),
  );
  $form['t4']['font']['block_title_font'] = array(
    '#type' => 'select',
    '#title' => t('Block titles'),
    '#default_value' => theme_get_setting('block_title_font'),
    '#options' => array(
      'btf-sss' => t('Helvetica Nueue, Trebuchet MS, Arial, Nimbus Sans L, FreeSans, sans-serif'),
      'btf-ssl' => t('Verdana, Geneva, Arial, Helvetica, sans-serif'),
      'btf-a'   => t('Arial, Helvetica, sans-serif'),
      'btf-ss'  => t('Garamond, Perpetua, Nimbus Roman No9 L, Times New Roman, serif'),
      'btf-sl'  => t('Baskerville, Georgia, Palatino, Palatino Linotype, Book Antiqua, URW Palladio L, serif'),
      'btf-m'   => t('Myriad Pro, Myriad, Arial, Helvetica, sans-serif'),
      'btf-l'   => t('Lucida Sans, Lucida Grande, Lucida Sans Unicode, Verdana, Geneva, sans-serif'),
    ),
  );
  $form['t4']['font']['font_size'] = array(
    '#type' => 'select',
    '#title' => t('Base Font Size'),
    '#default_value' => theme_get_setting('font_size'),
    '#description' => t('This sets a base font-size on the body element - all text will scale relative to this value.'),
    '#options' => array(
      'fs-smallest' => t('Smallest'),
      'fs-small'    => t('Small'),
      'fs-medium'   => t('Medium'),
      'fs-large'    => t('Large'),
      'fs-largest'  => t('Largest'),
    ),
  );
  // Breadcrumbs
  $form['t4']['breadcrumb'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breadcrumb Settings'),
    '#weight' => -80,
  );
  $form['t4']['breadcrumb']['breadcrumb_display'] = array(
    '#type' => 'select',
    '#title' => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_display'),
    '#options' => array(
      'yes' => t('Yes'),
      'no' => t('No'),
    ),
  );
  $form['t4']['breadcrumb']['breadcrumb_separator'] = array(
    '#type'  => 'textfield',
    '#title' => t('Breadcrumb separator'),
    '#description' => t('Text only. Dont forget to include spaces.'),
    '#default_value' => theme_get_setting('breadcrumb_separator'),
    '#size' => 8,
    '#maxlength' => 10,
    '#states' => array(
      'visible' => array(
          '#edit-breadcrumb-display' => array(
            'value' => 'yes',
          ),
      ),
    ),
  );
  $form['t4']['breadcrumb']['breadcrumb_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the "Home" link in breadcrumbs.'),
    '#default_value' => theme_get_setting('breadcrumb_home'),
    '#states' => array(
      'visible' => array(
          '#edit-breadcrumb-display' => array(
            'value' => 'yes',
          ),
      ),
    ),
  );
  $form['theme_settings']['#collapsible'] = TRUE;
  $form['theme_settings']['#collapsed'] = TRUE;
  $form['logo']['#collapsible'] = TRUE;
  $form['logo']['#collapsed'] = TRUE;
  $form['favicon']['#collapsible'] = TRUE;
  $form['favicon']['#collapsed'] = TRUE;
}