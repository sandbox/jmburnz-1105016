<?php if ($page['quintpanel_1'] || $page['quintpanel_2'] || $page['quintpanel_3'] || $page['quintpanel_4'] || $page['quintpanel_5']): ?>
<div id="quintpanel-wrapper" class="wrapper">
  <div class="container clearfix">
    <div class="inner <?php print $grid; ?>">

      <?php if ($page['quintpanel_1']): ?>
        <div class="quintpanel-1 clearfix grid-<?php print ($grid_int = '12') ? '6' : '3'; ?>">
          <?php print render($page['quintpanel_1']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['quintpanel_2']): ?>
        <div class="quintpanel-2 clearfix grid-<?php print ($grid_int = '12') ? '4' : '2'; ?>">
          <?php print render($page['quintpanel_2']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['quintpanel_3']): ?>
        <div class="quintpanel-3 clearfix grid-<?php print ($grid_int = '12') ? '4' : '2'; ?>">
          <?php print render($page['quintpanel_3']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['quintpanel_4']): ?>
        <div class="quintpanel-4 clearfix grid-<?php print ($grid_int = '12') ? '4' : '2'; ?>">
          <?php print render($page['quintpanel_4']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['quintpanel_5']): ?>
        <div class="quintpanel-5 clearfix grid-<?php print ($grid_int = '12') ? '6' : '3'; ?> last-col">
          <?php print render($page['quintpanel_5']); ?>
        </div>
      <?php endif; ?>

    </div>
  </div>
</div>
<?php endif; ?>
