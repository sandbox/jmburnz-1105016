<?php if ($page['bipanel_1'] || $page['bipanel_2']): ?>
<div id="bipanel-wrapper" class="wrapper">
  <div class="container clearfix">
    <div class="inner <?php print $grid; ?>">

      <?php if ($page['bipanel_1']): ?>
        <div class="bipanel-1 clearfix <?php print ns($grid, $page['bipanel_2'], $bipanel_panel_width); print $page['bipanel_2'] ? '' : ' last-col'; ?>">
          <?php print render($page['bipanel_1']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['bipanel_2']): ?>
        <div class="bipanel-2 clearfix <?php print ns($grid, $page['bipanel_1'], $bipanel_panel_width); ?> last-col">
          <?php print render($page['bipanel_2']); ?>
        </div>
      <?php endif; ?>

    </div>
  </div>
</div>
<?php endif; ?>