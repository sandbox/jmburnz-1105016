<?php if ($page['quadpanel_1'] || $page['quadpanel_2'] || $page['quadpanel_3'] || $page['quadpanel_4']): ?>
<div id="quadpanel-wrapper" class="wrapper">
  <div class="container clearfix">
    <div class="inner <?php print $grid; ?>">

      <?php if ($page['quadpanel_1']): ?>
        <div class="quadpanel-1 clearfix grid-<?php print $quadpanel_panel_width; ?>">
          <?php print render($page['quadpanel_1']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['quadpanel_2']): ?>
        <div class="quadpanel-2 clearfix grid-<?php print $quadpanel_panel_width; ?>">
          <?php print render($page['quadpanel_2']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['quadpanel_3']): ?>
        <div class="quadpanel-3 clearfix grid-<?php print $quadpanel_panel_width; ?>">
          <?php print render($page['quadpanel_3']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['quadpanel_4']): ?>
        <div class="quadpanel-4 clearfix grid-<?php print $quadpanel_panel_width; ?> last-col">
          <?php print render($page['quadpanel_4']); ?>
        </div>
      <?php endif; ?>

    </div>
  </div>
</div>
<?php endif; ?>
