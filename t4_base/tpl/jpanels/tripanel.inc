<?php if ($page['tripanel_1'] || $page['tripanel_2'] || $page['tripanel_3']): ?>
<div id="tripanel-wrapper" class="wrapper">
  <div class="container clearfix">
    <div class="inner <?php print $grid; ?>">

      <?php if ($page['tripanel_1']): ?>
        <div class="tripanel-1 clearfix grid-<?php print $tripanel_panel_width; ?>">
          <?php print render($page['tripanel_1']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['tripanel_2']): ?>
        <div class="tripanel-2 clearfix grid-<?php print $tripanel_panel_width; ?>">
          <?php print render($page['tripanel_2']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['tripanel_3']): ?>
        <div class="tripanel-3 clearfix grid-<?php print $tripanel_panel_width; ?> last-col">
          <?php print render($page['tripanel_3']); ?>
        </div>
      <?php endif; ?>

    </div>
  </div>
</div>
<?php endif; ?>
