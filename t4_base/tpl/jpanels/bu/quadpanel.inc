<?php if ($page['quadpanel_1'] || $page['quadpanel_2'] || $page['quadpanel_3'] || $page['quadpanel_4']): ?>
<div id="quadpanel-wrapper" class="wrapper">
  <div class="container clearfix">

    <?php if ($page['quadpanel_1']): ?>
      <div class="quadpanel-1 clearfix <?php print ns('grid-12', $page['quadpanel_2'], 3, $page['quadpanel_3'], 3, $page['quadpanel_4'], 3,  !$page['quadpanel_4'], 3); ?><?php if (!$page['quadpanel_2'] && !$page['quadpanel_3'] && !$page['quadpanel_4']) { print ' last-col'; } ?>">
        <?php print render($page['quadpanel_1']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['quadpanel_2']): ?>
      <div class="quadpanel-2 clearfix <?php print ns('grid-9', $page['quadpanel_3'], 3, $page['quadpanel_4'], 3, !$page['quadpanel_3'], 3); ?><?php if (!$page['quadpanel_3'] && !$page['quadpanel_4']) { print ' last-col'; } ?>">
        <?php print render($page['quadpanel_2']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['quadpanel_3']): ?>
      <div class="quadpanel-3 clearfix <?php print ns('grid-6', $page['quadpanel_4'], 3, !$page['quadpanel_4'], 3); ?><?php if (!$page['quadpanel_4']) { print ' last-col'; } ?>">
        <?php print render($page['quadpanel_3']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['quadpanel_4']): ?>
      <div class="quadpanel-4 clearfix grid-3 last-col">
        <?php print render($page['quadpanel_4']); ?>
      </div>
    <?php endif; ?>

  </div>
</div>
<?php endif; ?>
