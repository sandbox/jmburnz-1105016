<?php if ($page['tripanel_1'] || $page['tripanel_2'] || $page['tripanel_3']): ?>
<div id="tripanel-wrapper" class="wrapper">
  <div class="container clearfix">
    <div class="inner center clearfix">

    <?php if ($page['tripanel_1']): ?>
      <div class="tripanel-1 clearfix <?php print ns('grid-12', $page['tripanel_2'], 4, $page['tripanel_3'], 4, !$page['tripanel_3'], 4); ?><?php if (!$page['tripanel_2'] && !$page['tripanel_3']) { print ' last-col'; } ?>">
        <?php print render($page['tripanel_1']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['tripanel_2']): ?>
      <div class="tripanel-2 clearfix <?php print ns('grid-8', $page['tripanel_3'], 4); ?><?php if (!$page['tripanel_3']) { print ' last-col'; } ?>">
        <?php print render($page['tripanel_2']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['tripanel_3']): ?>
      <div class="tripanel-3 clearfix grid-4 last-col">
        <?php print render($page['tripanel_3']); ?>
      </div>
    <?php endif; ?>

    </div>
  </div>
</div>
<?php endif; ?>
