<?php if ($page['bipanel_nested_1'] || $page['bipanel_nested_2']): ?>
<div id="bipanel-nested" class="clearfix nested">
  <div class="inner grid-12">

    <?php if ($page['bipanel_nested_1']): ?>
      <div class="bipanel-nested-1 clearfix <?php print ns('grid-12', $page['bipanel_nested_2'], 6); print $page['bipanel_nested_2'] ? '' : ' last-col'; ?>">
        <?php print render($page['bipanel_nested_1']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['bipanel_nested_2']): ?>
      <div class="bipanel-nested-2 clearfix <?php print ns('grid-12', $page['bipanel_nested_1'], 6); ?> last-col">
        <?php print render($page['bipanel_nested_2']); ?>
      </div>
    <?php endif; ?>

  </div>
</div>
<?php endif; ?>
