


<?php if ($page['bipanel_1'] || $page['bipanel_2']): ?>
<div id="bipanel-wrapper" class="wrapper">
  <div class="container clearfix">

    <?php if ($page['bipanel_1']): ?>
      <div class="bipanel-1 clearfix <?php print ns('grid-12', $page['bipanel_2'], 6); print $page['bipanel_2'] ? '' : ' last-col'; ?>">
        <?php print render($page['bipanel_1']); ?>
      </div>
    <?php endif; ?>

    <?php if ($page['bipanel_2']): ?>
      <div class="bipanel-2 clearfix <?php print ns('grid-12', $page['bipanel_1'], 6); ?> last-col">
        <?php print render($page['bipanel_2']); ?>
      </div>
    <?php endif; ?>

  </div>
</div>
<?php endif; ?>