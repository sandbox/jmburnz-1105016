<?php
// Implementation of hook_theme()
function t4_theme() {
  $items = array();
  $items['fieldset'] = array(
    'arguments' => array('element' => array()),
    'template' => 'fieldset',
    'path' => drupal_get_path('theme', 't4') .'/tpl',
  );
  // Split out pager list into separate theme function.
  $items['pager_list'] = array('arguments' => array(
    'tags' => array(),
    'limit' => 10,
    'element' => 0,
    'parameters' => array(),
    'quantity' => 9,
  ));
  return $items;
}

// Conditionally load debug CSS
if (theme_get_setting('layout_debug_grid') == 1) {
  drupal_add_css(drupal_get_path('theme', 't4') . '/css/debug.css', array('group' => CSS_THEME));
}
// Add grids CSS
$grid = theme_get_setting('grid');
drupal_add_css(drupal_get_path('theme', 't4') . '/css/' . $grid . '.css', array('group' => CSS_THEME,));

// Split funtions and stuff into seperate files for eaiser house keeping.
include_once(drupal_get_path('theme', 't4') . '/inc/theme_function_overrides.inc');
include_once(drupal_get_path('theme', 't4') . '/inc/custom_functions.inc');
include_once(drupal_get_path('theme', 't4') . '/inc/preprocess_functions.inc');
include_once(drupal_get_path('theme', 't4') . '/inc/alters.inc');
