<?php
/**
 * Preprocess functions
 */
function t4_preprocess_html(&$vars) {
  global $theme;
  if (theme_get_setting('layout_type') == 'fixed') {
    $width = theme_get_setting('layout_width_fixed');
  }
  if (theme_get_setting('layout_type') == 'fluid') {
    $width = theme_get_setting('layout_width_fluid');
  }
  $layout_width = 'div.container{width:'. $width .';}';
  if (theme_get_setting('layout_minmax_toggle') == 1) {
    $min_width = theme_get_setting('layout_min_width');
    $max_width = theme_get_setting('layout_max_width');
    $min_max_width = 'div.container{min-width:'. $min_width . '; max-width:' . $max_width . ';}';
  }
  if (theme_get_setting('layout_type') == 'fluid') {
    $layout = $layout_width;
  }
  if (theme_get_setting('layout_type') == 'fixed') {
    $layout = $layout_width;
  }
  if (theme_get_setting('layout_type') == 'fluid' && theme_get_setting('layout_minmax_toggle') == 1 && $width == '100%') {
    $layout = $layout_width . $min_max_width;
  }
  drupal_add_css($layout, array('group' => CSS_THEME, 'type' => 'inline'));

  if(theme_get_setting('layout_debug_grid') == 1) {
    $vars['classes_array'][] = 'show-grid';
  }
  $vars['classes_array'][] = drupal_html_class($theme);
  $vars['classes_array'][] = theme_get_setting('base_font');
  $vars['classes_array'][] = theme_get_setting('site_name_font');
  $vars['classes_array'][] = theme_get_setting('site_slogan_font');
  $vars['classes_array'][] = theme_get_setting('page_title_font');
  $vars['classes_array'][] = theme_get_setting('node_title_font');
  $vars['classes_array'][] = theme_get_setting('comment_title_font');
  $vars['classes_array'][] = theme_get_setting('block_title_font');
  $vars['classes_array'][] = theme_get_setting('font_size');
}

function t4_preprocess_region(&$vars) {
}

function t4_preprocess_page(&$vars) {
  $vars['layout_path'] = drupal_get_path('theme', 't4') . '/tpl/layouts/';
  
  // Get theme settings and populate some variables
  $grid = theme_get_setting('grid');
  $sidebar_layout = theme_get_setting('sidebar_layout');
  $sidebar_first_width = theme_get_setting('sidebar_first_width');
  $sidebar_second_width = theme_get_setting('sidebar_second_width');

  // Set variables for the grid type
  $grid_int = substr($grid, -2);
  $vars['grid_int'] = $grid_int;
  $vars['grid'] = $grid;

  // Build the layout
  $main_content_width = ns($grid, block_list('sidebar_first'), $sidebar_first_width, block_list('sidebar_second'), $sidebar_second_width);

  // conditionally add last-col classes
  $last_col = block_list('sidebar_second') ? '' : ' last-col';

  // split sidebar layout, content source ordered.
  if ($sidebar_layout == 0) {

    // main content - this needs to push away from the left margin
    $main_content_offset = ns('push-' . $sidebar_first_width, !block_list('sidebar_first'), $sidebar_first_width);
    $vars['main_content_layout'] = $main_content_width . ' ' . $main_content_offset  . $last_col;

    // sidebar first - this needs to be pulled into position
    $sidebar_pull = ns('pull-'. $grid_int, block_list('sidebar_second'), $sidebar_second_width);
    $vars['sidebar_first_layout'] = 'grid-' . $sidebar_first_width . ' ' . $sidebar_pull;

  }
  // both sidebars on the right
  else {
    if (block_list('sidebar_first')) {
      $vars['main_content_layout'] = $main_content_width;
    }
    else {
      $vars['main_content_layout'] = $main_content_width . $last_col;
    }
    $vars['sidebar_first_layout'] = 'grid-' . $sidebar_first_width . $last_col;
  }
  // sidebar second never moves, its always last
  $vars['sidebar_second_layout'] = 'grid-' .$sidebar_second_width . ' last-col';
  
  // Set some simple vars for jpanels
  $vars['bipanel_panel_width'] = $grid_int/2;
  $vars['tripanel_panel_width'] = $grid_int/3;
  $vars['quadpanel_panel_width'] = $grid_int/4;
}

function t4_preprocess_node(&$vars) {
  $vars['title_attributes_array']['class'][] = 'node-title';
  $vars['content_attributes_array']['class'][] = 'node-content';
}

function t4_preprocess_comment(&$vars) {
  $vars['title_attributes_array']['class'][] = 'comment-title';
  $vars['content_attributes_array']['class'][] = 'comment-content';
  $uri = entity_uri('comment', $vars['comment']);
  $uri['options'] += array('attributes' => array('rel' => 'bookmark'));
  $vars['title'] = l($vars['comment']->subject, $uri['path'], $uri['options']);
  $vars['permalink'] = l(t('Permalink'), $uri['path'], $uri['options']);
  $vars['created'] = '<span class="date-time permalink">' . l($vars['created'], $uri['path'], $uri['options']) . '</span>';
  $vars['datetime'] = format_date($vars['comment']->created, 'custom', 'c');
  $vars['unpublished'] = '';
  if ($vars['status'] == 'comment-unpublished') {
    $vars['unpublished'] = '<div class="unpublished">' . t('Unpublished') . '</div>';
  }
}

function t4_preprocess_comment_wrapper(&$vars) {
  if ($vars['node']->type == 'forum') {
    $vars['title_attributes_array']['class'][] = 'element-invisible';
  }
}

function t4_preprocess_block(&$vars) {
  $vars['title_attributes_array']['class'][] = 'block-title';
  $vars['content_attributes_array']['class'][] = 'block-content';
  if ($vars['block']->region == 'menu_bar') {
    $vars['title_attributes_array']['class'][] = 'element-invisible';
  }
  //$nav_blocks = array('navigation', 'main-menu', 'management', 'user-menu', 'forum');
  if (in_array($vars['block']->delta, array_keys(menu_get_menus()))) {
    $vars['theme_hook_suggestions'][] = 'block__menu';
  }
}

function t4_preprocess_aggregator_item(&$vars) {
  $item = $vars['item'];
  $vars['datetime'] = format_date($item->timestamp, 'custom', 'c');
}

function t4_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

function t4_preprocess_fieldset(&$vars) {
  $element = $vars['element'];
  $vars['attributes'] = $element['#attributes'];
  $vars['attributes']['id'][] =  $element['#id'];
  $vars['attributes']['class'][] = 'fieldset';
  $vars['attributes']['class'][] = 'form-wrapper';
  if (!empty($element['#title'])) {
    $vars['attributes']['class'][] = 'titled';
  }
  $description = !empty($element['#description']) ? "<div class='description'>{$element['#description']}</div>" : '';
  $children = !empty($element['#children']) ? $element['#children'] : '';
  $value = !empty($element['#value']) ? $element['#value'] : '';
  $vars['content'] = $description . $children . $value;
  $vars['title'] = !empty($element['#title']) ? $element['#title'] : '';
  $vars['hook'] = 'fieldset';
  //kpr($element); // debug with Devel module and Krumo
}
