<?php
/**
 * Implimentation of hook_form_system_theme_settings_alter().
 *
 * USAGE:
 * 1 - To use this file replace "t4_subtheme" with the name of
 *     your theme in the function below.
 * 2 - Set 'style_enable_schemes' to 'on' in your themes info file (its near the bottom of that file).
 * 3 - Build or un-comment the forms. The Style Schemes form is ready to use, just un-comment it.
 */
function t4_subtheme_form_system_theme_settings_alter(&$form, &$form_state)  {

  // Build a form for the custom theme settings using Drupals Form API.
  // Style Schemes: we can extend the base themes setting by maintaing the same form structure.
  /* -- Delete this line if you want to use Style Schemes

  if (theme_get_setting('style_enable_schemes') == 'on') {
    $form['t4']['style'] = array(
      '#type' => 'fieldset',
      '#title' => t('Style Settings'),
      '#weight' => -70,
    );
    $form['t4']['style']['style_schemes'] = array(
      '#type' => 'select',
      '#title' => t('Styles'),
      '#default_value' => theme_get_setting('style_schemes'),
      '#options' => array(
        'none' => t('None'),
        'style-default.css' => t('Default Style'),
        // 'my-style.css' => t('My Style'), // Add your schemes, these MUST be placed in your subthemes /schemes directory!
      ),
    );
    $form['t4']['style']['style_enable_schemes'] = array(
      '#type' => 'hidden',
      '#value' => theme_get_setting('style_enable_schemes'),
    );
  }

  // */
}
